create table `client` (
    id int(11) not null AUTO_INCREMENT,
    full_name varchar(128) not null,
    abbreviation varchar(6) not null,
    primary key (`id`)
);
