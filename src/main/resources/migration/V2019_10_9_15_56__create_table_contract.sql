create table `contract` (
    contract_id int(11) not null AUTO_INCREMENT,
    name varchar(128) not null,
    cid int(11) not null,
    primary key (contract_id),
    foreign key (cid) references client(id)
);
