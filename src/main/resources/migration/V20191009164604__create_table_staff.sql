create table `staff`(
    `staff_id` int(11) not null AUTO_INCREMENT,
    `first_name` varchar(128) not null,
    `last_name` varchar(128) not null,
    `office_id` int(11) not null,
    primary key (`staff_id`),
    foreign key (`office_id`) references offices(`office_id`)
);


