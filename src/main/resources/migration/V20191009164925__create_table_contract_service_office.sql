create table `contract_service_office`(
    `contract_service_office_id` int(11) not null AUTO_INCREMENT,
    `contract_id` int(11) not null,
    `service_id` int(11) not null,
    `admin_id` int(11) not null,
    primary key (`contract_service_office_id`),
    foreign key (`contract_id`) references contract(`contract_id`),
    foreign key (`service_id`) references services(`id`),
    foreign key (`admin_id`) references staff(`staff_id`)
);
